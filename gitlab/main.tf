provider "gitlab" {
  token = "${var.gitlab_token}"
}

resource "gitlab_project" "terraform" {
  name             = "terraform-rebrain"
  description      = "My awesome terraform"
  visibility_level = "public"
  default_branch   = "master"
}

resource "gitlab_deploy_key" "key" {
  project    = "isaidashev/terraform-rebrain"
  title      = "deploy key"
  key        = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCfd8jqwrySMoHV6XsQ0enSkKsSnh+JBTdqsCC0SLvY/AmtSUPxWW6V10pW6lQECyOHgHJFTL+n6uk2YaR+0EUD49KhN6CgHHEDBC2TQTKpMys1E7q7nyk59df1EGS1lyGSNqHUH//X/1MGyWDcfV2taSrT4RZiFDySTZMW6NGH7Yh+cjkopYSHauG3rVCHZ+y+gzJBt6a2wieL1TnrCR0sFZ+mP11S7OMtlbFvBGWBt/DbAeoMmzF1tn4NM77vIqzYII/rZmW+bpw+RLawwVWUeCFpV7mK0ol0nqdPX+PmIKzdwpRw9VDTmaUdrlD1mHtgC/Hw1053wVnnjCoNtyir appuser"
  can_push   = "True"
  depends_on = ["gitlab_project.terraform"]
}
