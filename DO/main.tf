provider "digitalocean" {
  token = "${var.do_token}"
}

# resource "digitalocean_volume" "vol" {
#   region                  = "FRA1"
#   name                    = "isaidashev-volume1"
#   size                    = 25
#   initial_filesystem_type = "ext4"
#   description             = "For Rebrain"
# }

resource "digitalocean_tag" "tag" {
  name = "isaidashev"
}

resource "digitalocean_ssh_key" "default" {
  name       = "Terraform Example"
  public_key = "${var.ssh_fingerprint2}"
}

resource "digitalocean_droplet" "host" {
  image    = "ubuntu-18-04-x64"
  name     = "isaidashev-Rebrain-1"
  region   = "FRA1"
  size     = "s-1vcpu-1gb"
  ssh_keys = ["${digitalocean_ssh_key.default.fingerprint}"]
  tags     = ["${digitalocean_tag.tag.id}"]
}
