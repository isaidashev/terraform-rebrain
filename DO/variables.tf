variable do_token {
  description = "Digital Ocean token"
}

variable "ssh_fingerprint1" {
  description = "ssh pub key 1"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDNm4pL78YE6j9FTD6lGPzGIXl94q2118orskmYoSfr5qzZspXhhLlMu2Y9R20/8KVns1T8j9Q/fb9X33MtjuPRoNmz5LPIqoIYblbujdFqt+5vpz2YbfHPEBC5GrN2XHw4wFzyXCki5DaYC6Ktj2brJGUJomrIc2hwzK+wV2ncGLZv73E1+sDUdGuuLFeU60lvrK6fp03KN3Dyouc61RDPmG81omA5obcf4jBdA6FjoOpVq64XYqR0kzUhM2DXKsnagfkV9oFBfDdz3U+JZRz7ubR4iPtojq5LyQE8Ah3q2CDPxrEBKkJbglRoPBRQ0NGtyNH83HfIPZctkLMx8ja3"
}

variable "ssh_fingerprint2" {
  description = "ssh pub key 2"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDF8q2ywqK9KvvhQDS3gYxtrPOildTIerG7x7ny9+LxSMQZsUJI9iB/Lkne321KXXJVCLMFnnJ3cBZMAu5cIXn9HcAxz58/x42FituTRNBebLC81V9Fxl48s+n1AYyV3RKPsEXHifDzMjRovXQy0EXgrFuiNBWfqUcueEkFjVjp+nXRQ0vRFIfMCiR4p/UGHVaAIXybbRo1TH98zVQZ1Xmeo8iKckumTUl0hX3ks1wDpM6rAw5TD8LHqHwkHJZk8BXk6cJuX5+Mf18+eIT7L3rUMYiexa3d+LoLuzzvBCodSkW3YXjsTRDwmvva4/u8MYCIEG+JmHN/WEzSAGNkrIs9"
}
