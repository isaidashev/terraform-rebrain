provider "vscale" {
  token = "${var.vscale_token}"
}

resource "vscale_ssh_key" "isaidashev" {
  name = "ssh-key-bar-isaidashev"
  key  = "${var.vscale_public_key}"
}

resource "vscale_scalet" "vscale_host" {
  name      = "vscale_host_name"
  make_from = "ubuntu_14.04_64_002_master"
  rplan     = "small"
  location  = "spb0"
  ssh_keys  = ["${vscale_ssh_key.isaidashev.id}"]
}

resource "vscale_domain" "domain" {
  name = "isaidashev.ru"
}

resource "vscale_record" "dns" {
  domain  = "${vscale_domain.domain.id}"
  name    = "${vscale_domain.domain.name}"
  content = "${vscale_scalet.vscale_host.public_address}"
  email   = "isaidashev@gmail.com"
  type    = "A"
  ttl     = "300"
}
