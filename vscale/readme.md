Предварительные настройки:

```
brew install go
sudo vi ~/.bash_profile
export GOPATH=$HOME/go
PATH=$PATH:$GOPATH/bin
mkdir $HOME/[go,bin]
go get github.com/hashicorp/terraform
go get github.com/vscale/go-vscale
go get github.com/pkg/errors
git clone git@github.com:burkostya/terraform-provider-vscale.git $HOME/bin/
cd $GOPATH/bin/terraform-provider-vscale
go build .
cp terraform-provider-vscale ~/.terraform.d/plugins/
```
