provider "vscale" {
  token = "${var.vscale_token}"
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "vscale_ssh_key" "isaidashev" {
  name = "ssh-key-bar-isaidashev"
  key  = "${var.vscale_public_key}"
}

resource "random_string" "password" {
  count   = "${length(var.devs)}"
  length  = 16
  special = true
}

resource "vscale_scalet" "vscale_host" {
  count     = "${length(var.devs)}"
  name      = "${var.devs[count.index]}"
  make_from = "ubuntu_14.04_64_002_master"
  rplan     = "small"
  location  = "spb0"
  ssh_keys  = ["${vscale_ssh_key.isaidashev.id}"]

  provisioner "local-exec" {
    command = "ssh-keyscan -H ${self.public_address} >> ~/.ssh/know_hosts"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo echo 'root:${element(random_string.password.*.result, count.index)}' | chpasswd",
    ]

    connection {
      type        = "ssh"
      private_key = "${file(var.private_key_path)}"
      user        = "${var.ssh_user}"
      agent       = true
    }
  }
}

data "aws_route53_zone" "zone" {
  name         = "devops.rebrain.srwx.net."
  private_zone = false
}

resource "aws_route53_record" "www" {
  count      = "${length(var.devs)}"
  zone_id    = "${data.aws_route53_zone.zone.zone_id}"
  name       = "${var.devs[count.index]}.${data.aws_route53_zone.zone.name}"
  type       = "A"
  ttl        = "300"
  records    = ["${vscale_scalet.vscale_host.*.public_address}"]
  depends_on = ["vscale_scalet.vscale_host"]

  provisioner "local-exec" {
    command = " echo '${self.name} ${element(vscale_scalet.vscale_host.*.public_address, count.index)} ${element(random_string.password.*.result, count.index)}' >> devs.txt"
  }
}
