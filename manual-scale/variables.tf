variable "devs" {
  type    = "list"
  default = ["dev1.isaidashev", "dev2.isaidashev"]
}

variable "project" {
  default = "isaidashev"
}

variable vscale_token {
  description = "Vscale token"
}

variable "vscale_public_key" {
  description = "ssh pub key 1"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDF8q2ywqK9KvvhQDS3gYxtrPOildTIerG7x7ny9+LxSMQZsUJI9iB/Lkne321KXXJVCLMFnnJ3cBZMAu5cIXn9HcAxz58/x42FituTRNBebLC81V9Fxl48s+n1AYyV3RKPsEXHifDzMjRovXQy0EXgrFuiNBWfqUcueEkFjVjp+nXRQ0vRFIfMCiR4p/UGHVaAIXybbRo1TH98zVQZ1Xmeo8iKckumTUl0hX3ks1wDpM6rAw5TD8LHqHwkHJZk8BXk6cJuX5+Mf18+eIT7L3rUMYiexa3d+LoLuzzvBCodSkW3YXjsTRDwmvva4/u8MYCIEG+JmHN/WEzSAGNkrIs9"
}

variable "private_key_path" {
  default = "~/.ssh/id_rsa"
}

variable "aws_access_key" {
  description = "Access key"
}

variable "aws_secret_key" {
  description = "Secret key"
}

variable "ssh_user" {
  default = "root"
}
