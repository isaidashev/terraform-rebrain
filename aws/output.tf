output "public_ip" {
  value = "${vscale_scalet.vscale_host.*.public_address}"
}

output "record_name" {
  value = "${aws_route53_record.www.*.name}"
}
