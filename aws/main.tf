provider "vscale" {
  token = "${var.vscale_token}"
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "vscale_ssh_key" "isaidashev" {
  name = "ssh-key-bar-isaidashev"
  key  = "${var.vscale_public_key}"
}

resource "vscale_scalet" "vscale_host" {
  count     = "${var.instance_count}"
  name      = "${var.project}-${count.index}"
  make_from = "ubuntu_14.04_64_002_master"
  rplan     = "small"
  location  = "spb0"
  ssh_keys  = ["${vscale_ssh_key.isaidashev.id}"]
}

data "aws_route53_zone" "zone" {
  name         = "devops.rebrain.srwx.net."
  private_zone = false
}

resource "aws_route53_record" "www" {
  count   = "${var.instance_count}"
  zone_id = "${data.aws_route53_zone.zone.zone_id}"
  name    = "${var.project}-${count.index}.${data.aws_route53_zone.zone.name}"
  type    = "A"
  ttl     = "300"
  records = ["${vscale_scalet.vscale_host.*.public_address}"]
}
